// ------------------------------------------------------------------------------
// Copyright 2021 Uwe Arzt, mail@uwe-arzt.de
// SPDX-License-Identifier: Apache-2.0
// ------------------------------------------------------------------------------

use hal::{Delay, I2cdev};
use linux_embedded_hal as hal;

use mprls::{Address, PressureUnit, MPR};

use std::thread;
use std::time::Duration;

fn main() -> Result<(), std::io::Error> {
    let i2c = I2cdev::new("/dev/i2c-1").unwrap();
    let mut mpr = MPR::new(i2c, Delay, Address::Standard);

    loop {
        println!("-------------------------------------------------------------------------");
        println!(
            "pressure PSI : {}",
            mpr.get_pressure_wait(PressureUnit::PSI).unwrap()
        );
        println!(
            "pressure PA  : {}",
            mpr.get_pressure_wait(PressureUnit::PA).unwrap()
        );
        println!(
            "pressure KPA : {}",
            mpr.get_pressure_wait(PressureUnit::KPA).unwrap()
        );
        println!(
            "pressure TORR: {}",
            mpr.get_pressure_wait(PressureUnit::TORR).unwrap()
        );
        println!(
            "pressure INHG: {}",
            mpr.get_pressure_wait(PressureUnit::INHG).unwrap()
        );
        println!(
            "pressure ATM : {}",
            mpr.get_pressure_wait(PressureUnit::ATM).unwrap()
        );
        println!(
            "pressure BAR : {}",
            mpr.get_pressure_wait(PressureUnit::BAR).unwrap()
        );

        let sleeptime = Duration::from_secs(4);
        thread::sleep(sleeptime);
    }
}
