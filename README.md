# [mprls](https://crates.io/crates/mprls)

[![Apache licensed](https://img.shields.io/badge/license-Apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0)
[![crates.io](https://meritbadge.herokuapp.com/mpr)](https://crates.io/crates/mprls)

Rust driver for Honeywell MPR pressure sensor.

## Documentation

Read the detailed documentation [here](https://docs.rs/mprls/)

## Build

### Raspberry Pi

```bash
cargo build --example raspberrypi
cargo run --example raspberrypi
```

## License

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Resources

- [Datasheet](https://cdn.sparkfun.com/assets/2/e/8/0/9/honeywell-mpr-datasheet.pdf)
- [Evaluation Board](https://www.sparkfun.com/products/16476)
